Source: php-league-flysystem
Section: php
Priority: optional
Maintainer: Debian PHP PEAR Maintainers <pkg-php-pear@lists.alioth.debian.org>
Uploaders:
 Robin Gustafsson <robin@rgson.se>,
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-phpcomposer,
 php-guzzlehttp-psr7 <!nocheck>,
 php-league-mime-type-detection <!nocheck>,
 phpab,
 phpunit <!nocheck>,
Standards-Version: 4.7.1
Homepage: https://flysystem.thephpleague.com/
Vcs-Browser: https://salsa.debian.org/php-team/pear/php-league-flysystem
Vcs-Git: https://salsa.debian.org/php-team/pear/php-league-flysystem.git
Rules-Requires-Root: no

Package: php-league-flysystem
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
 ${phpcomposer:Debian-require},
Suggests:
 ${phpcomposer:Debian-suggest},
Replaces:
 ${phpcomposer:Debian-replace},
Breaks:
 ${phpcomposer:Debian-conflict},
 ${phpcomposer:Debian-replace},
Provides:
 php-league-flysystem-local (= ${binary:Version}),
 ${phpcomposer:Debian-provide},
Description: filesystem abstraction offering one API to many filesystems
 Flysystem is a filesystem abstraction which allows you to easily swap out a
 local filesystem for a remote one.
 .
 Goals:
  * Have a generic API for handling common tasks across multiple file
    storage engines.
  * Have consistent output which you can rely on.
  * Integrate well with other packages and frameworks.
  * Be cacheable.
  * Emulate directories in systems that support none, like AwsS3.
  * Support third party plugins.
  * Make it easy to test your filesystem interactions.
  * Support streams for big file handling.
